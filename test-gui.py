import unittest
from os import environ
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

WEBSITE = "http://www.python.org"
is_local = bool(environ.get("IS_LOCAL", ""))

class TestPythonWebsite(unittest.TestCase):

    def setUp(self):
        if is_local:
            self.driver = webdriver.Chrome()
        else:
            ops = Options()
            ops.add_argument("--no-sandbox")
            ops.add_argument("--disable-dev-shm-usage")
            self.driver = webdriver.Remote(
                command_executor="http://selenium__standalone-chrome:4444/wd/hub",
                options=ops,
            )

    def test_search(self):
        driver = self.driver
        driver.get(WEBSITE)
        el = driver.find_element_by_name("q")
        el.send_keys("pycon")
        el.send_keys(Keys.RETURN)
        self.assertNotIn("No results found.", driver.page_source)

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()
